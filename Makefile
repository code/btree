.POSIX:
.PHONY: all clean
ALL = btree-int btree-str
all: $(ALL)
clean:
	rm -f -- $(ALL)
