#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
	char *v;
	struct node *l;
	struct node *r;
};

struct node *cn(char *);
void an(struct node *, struct node *);
void pn(struct node *);
void fn(struct node *);

struct node *cn(char *s) {
	struct node *n;
	char *d;

	n = malloc(sizeof *n);
	if (!n)
		return NULL;
	d = malloc(strlen(s) + 1);
	if (!d)
		return NULL;
	strcpy(d, s);
	n->v = d;
	n->l = n->r = NULL;
	return n;
}

void an(struct node *r, struct node *n) {
	if (!r || !n)
		return;
	if (strcmp(n->v, r->v) > 0) {
		if (r->r)
			an(r->r, n);
		else
			r->r = n;
	}
	else {
		if (r->l)
			an(r->l, n);
		else
			r->l = n;
	}
	return;
}

void pn(struct node *n) {
	if (!n)
		return;
	if (n->l)
		pn(n->l);
	fprintf(stdout, "%s\n", n->v);
	if (n->r)
		pn(n->r);
	return;
}

void fn(struct node *n) {
	if (!n)
		return;
	if (n->l)
		fn(n->l);
	if (n->r)
		fn(n->r);
	free(n->v);
	free(n);
	return;
}

int main(int argc, char **argv) {
	struct node *r, *n;

	r = NULL;
	for (argv++; *argv; argv++) {
		n = cn(*argv);
		if (!n)
			break;
		if (r)
			an(r, n);
		else
			r = n;
	}

	pn(r);

	fn(r);

	exit(EXIT_SUCCESS);
}
